import React from 'react'
import {
  Linking,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
  ActivityIndicator,
} from 'react-native';
import axios from 'axios';

class ConnectionsRow extends React.Component {

  constructor(props){
    super(props)
    this.handleClick = this.handleClick.bind(this)
  };

  handleClick(e){
    this.props.navigator.navigate('Connection', {connection: this.props.connection})
  };

  render(){
    // const { navigate } = this.props.navigation;
    return(
      <TouchableOpacity
        style={styles.optionsContainer}
        onPress={this.handleClick}>
        <View style={styles.option}>
          <View style={styles.optionIconContainer}>
          </View>
          <View style={styles.optionTextContainer}>
            <Text style={styles.optionText}>
              {this.props.connection.station.name} > {this.props.connection.connected_station.name}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
}

export default class ConnectionsListView extends React.Component {
  state = {connections: []}

  componentWillMount(){
    axios.get("https://private-77d045-railsfox.apiary-mock.com/v1/connections")
      .then(response => this.setState({connections: response.data}));
  }

  render() {
    console.log(this.state)
    if (this.state.connections.length > 0){
      return (
        <View>
          {this.state.connections.map(connection => <ConnectionsRow navigator={this.props.navigator} connection={connection} key={connection.id}/>)}
        </View>
      );
    } else {
      return (
        <View>
          <ActivityIndicator />
          <Text style={styles.loading_text}>Pobieranie połączeń</Text>
        </View>
      );
    }
  }
}


const styles = StyleSheet.create({
  loading_text:{
    textAlign: 'center',
    fontSize: 10,
  },
  container: {
    flex: 1,
    paddingTop: 15,
  },
  optionsTitleText: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 9,
    marginBottom: 12,
  },
  optionsContainer: {
    borderBottomWidth: 0,
    borderBottomColor: '#EDEDED',
  },
  optionIconContainer: {
    marginRight: 9,
  },
  option: {
    flexDirection: 'row',
    backgroundColor: 'rgba(0,0,0,0.0)',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
  },
  optionText: {
    fontSize: 15,
    marginTop: 1,
  },
});
