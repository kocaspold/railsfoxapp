import React from 'react';
import {
  Image,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {Text, Divider} from 'react-native-elements'
import CoursesListView from '../views/CoursesListView';

export default class ConnectionScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.connection.station.name + " > " + navigation.state.params.connection.connected_station.name,
    headerStyle: {backgroundColor: '#ed6434'},
    headerTitleStyle: {color: "#fff"},
    headerTintColor: "#fff",
  })

  render() {
    return (
      <View style={styles.container}>
        <CoursesListView connection={this.props.navigation.state.params.connection}/>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  appIcon: {
    width: 100,
    height: 120,
    resizeMode: 'contain',
  },
  infoHeader: {
    marginTop: 10,
  }
});
